/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import { StyleSheet, Text, View } from 'react-native';
import firebase from "firebase";

import {Header, Spinner} from './components/common';
import { LoginForm, Logout } from "./components/auth";

class App extends Component {
  state= {
    authenticated: null
  }
  componentDidMount() {
      var fireBaseConfig = { 
          apiKey: 'AIzaSyCgwKb1DtlCCJ20Hfu1KG1jcFT3N4fm5KY', 
          authDomain: 'js-authentication.firebaseapp.com', 
          databaseURL: 'https://js-authentication.firebaseio.com', 
          projectId: 'js-authentication', 
          storageBucket: 'js-authentication.appspot.com', 
          messagingSenderId: '549203168365' 
      };
      firebase.initializeApp(fireBaseConfig);
      
      firebase.auth().onAuthStateChanged((user) => {
        if(user) {
          this.setState({authenticated: true})
        } else {
          this.setState({ authenticated: false });
        }
      })
  }
  
  renderComponent() {
    switch (this.state.authenticated) {
      case true:
        return <Logout />;
        break;
      case false:
        return <LoginForm />;
        break;
      default:
        return <Spinner size='large' />;
        break;
    }
  }
  
  render() {
    const { containerStyle } = styles;
    return <View style={{ flex: 1 }}>
        <Header title="Authentication!" />
        {this.renderComponent()}
      </View>;
  }
}

const styles = StyleSheet.create({
});

export default App;