import React, { Component } from "react";
import { StyleSheet, Text} from "react-native";
import firebase from "firebase";
import {Card, CardSection, Button, Input, Spinner} from '../common'

class LoginForm extends Component {
  state = {
    email: '',
    password: '',
    error: '',
    loading: false
  };

  onLoginPress = () => {
    const {email, password} = this.state;
    this.setState({ error: '', loading: true });
    firebase.auth().signInWithEmailAndPassword(email, password)
    .then(()=>{
        this.onLoginSuccess();
    })
    .catch((e) => {
      firebase.auth().createUserWithEmailAndPassword(email, password)
      .catch(() => {
        this.onLoginFail();
      });
    });
  };

  onLoginSuccess() {
    this.setState({
      email: '',
      password: '',
      error: '',
      loading: false
    });
  }

  onLoginFail() {
    this.setState({
      error: 'Authentication Failed',
      loading: false
    });
  }

  renderLoginButton() {
    if(this.state.loading) {
      return <Spinner size='small' />;
    }
    return <Button onPress={() => this.onLoginPress()}>Login</Button>;
 
  }
  render() {
    const { errorTextStyle } = styles;
    return <Card>
        <CardSection>
          <Input 
            value={this.state.email} 
            onChangeText={email => this.setState({email})} 
            label='eMail' 
            placheholder = 'racksen@gmail.com'
            autoCorrect
          />
        </CardSection>
        <CardSection>
          <Input 
            value={this.state.password} 
            onChangeText={password => this.setState({password})} 
            label='password' 
            placheholder = 'Your pa$$W0rd'
            autoCorrect
            secureTextEntry
          />
        
        </CardSection>
        <Text style={errorTextStyle}>{this.state.error}</Text>
        <CardSection>
        {this.renderLoginButton()}
        </CardSection>
      </Card>;
  }
}

const styles = StyleSheet.create({
  errorTextStyle: {
    fontSize: 20,
    alignSelf: 'center',
    fontWeight: "bold",
    color: 'red'
  },
});

export { LoginForm };
