import React from 'react';
import { TextInput, View, Text, StyleSheet } from "react-native";

const Input = ({ label, value, onChangeText, autoCorrect, placheholder, secureTextEntry }) => {
  const { containerStyle, labelStyle, inputStyle } = styles;
  return (
    <View style={containerStyle}>
      <Text style={labelStyle}>{label}</Text>
      <TextInput
        value={value}
        onChangeText={onChangeText}
        style={inputStyle}
        autoCorrect={autoCorrect}
        placeholder={placheholder}
        secureTextEntry = {secureTextEntry}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
    flexDirection: "row",
    height: 40,
    alignItems: 'center'
  },
  labelStyle: {
    flex: 1,
    fontSize: 18,
    fontWeight: "bold",
    paddingLeft: 10
  },
  inputStyle: {
    flex: 2,
    color: "#000",
    fontSize: 18,
    paddingRight: 5,
    paddingLeft: 5,
    lineHeight: 23,
    height: 50,
    width: 400
  }
});
export {Input}